module.exports = [
  'id',
  'name',
  'icon',
  'link',
  'description',
  'privacy',
  'members.limit(0).summary(true)',
  'feed.limit(10)'
];

