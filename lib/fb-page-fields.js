module.exports = [
  'id',                   // Facebook page ID
  'about',                // Page information
  'can_post',             // Shows if it's possible to post to page
  'picture.type(large)',  // Page icon
  'current_location',     // Location related to the page
  'description',          // The description of the page
  'email',                // Contact email
  'feed.limit(10)',        // The feed of the Facebook page
  'general_info',         // General information about the page
  'is_verified',          // Shows of the page os having an authentic identity
  'engagement',           // # of page followers
  'link',                 // Page's Facebook URL
  'location',             // The location of the page
  'name',                 // The name of the page
  'parent_page',          // Shows if there's some parent Facebook page for this page
  'phone',                // Contact phone
  'single_line_address',  // The page address if any
  'talking_about_count',  // # of people talking about this Page
  'username',             // The alias of the Facebook page
  'website',              // The URL of the Page's website
];
