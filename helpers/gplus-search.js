/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
/* eslint new-cap: 0 */
'use strict';

const _ = require('lodash');
const cld = require('cld');
const czechStem = require('czech-stemmer');
const moment = require('moment');
const natural = require('natural');
const ukrStem = require('ukrstemmer');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});


const EnhancedSet = require('./enhanced-set');
const GPlusClient = require('./gplus-api');
const Post = require('./post');
const gp = new GPlusClient(process.env.GOOGLE_PLUS_TOKEN);

class GPlusSearch {
  /**
   * @param parameters - The set of parameters defining an instance of analysis.
   * @param {string[]} parameters.keys - Secondary search keys.
   * @param {!string} parameters.query - The main search key.
   * @param {?string} [parameters.locale=null] - ISO 3166 Alpha-2 country code.
   * @param {string[]} [parameters.inclusions=[]] - The set of plus-words.
   * @param {string[]} [parameters.exclusions=[]] - The set of minus-words.
   */
  constructor(parameters) {
    const { query, locale = 'global', period = 'y', keys = [], settings = {} } = parameters;
    const { include = [], exclude = [] } = settings;

    /** Converts either items or set of items to lower case */
    function lowerCase(item) {
      if (typeof item === 'string') {
        return item.toLocaleLowerCase();
      }
      return _.map(item, element => element.toLocaleLowerCase());
    }

    this._exclusions = lowerCase(exclude);
    this._excsTokens = new EnhancedSet();
    this._inclusions = lowerCase(include);
    this._incsTokens = new EnhancedSet();
    this._keywords = lowerCase(keys);
    this._keysTokens = new EnhancedSet();
    this._language = null;
    this._locale = locale;
    this._period = period;
    this._query = lowerCase(query);
    this._queryTokens = new EnhancedSet();
    this._results = [];
  }

  /**
   * Get analysis minus-words.
   * @returns {String[]} The minus-words.
   */
  get exclusions() {
    return this._exclusions;
  }

  /**
   * Set tokenized version of minus-words.
   * @param {string[]} words - The minus-words to store.
   */
  set excsTokens(words) {
    this._excsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of minus-words.
   * @returns {string[]} Tokenized minus-words.
   */
  get excsTokens() {
    return [...this._excsTokens];
  }

  /**
   * Get analysis plus-words.
   * @returns {String[]} The plus-words.
   */
  get inclusions() {
    return this._inclusions;
  }

  /**
   * Set tokenized version of plus-words.
   * @param {string[]} words - The plus-words to store.
   */
  set incsTokens(words) {
    this._incsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of plus-words.
   * @returns {string[]} Tokenized plus-words.
   */
  get incsTokens() {
    return [...this._incsTokens];
  }

  /**
   * Get analysis additional keywords.
   * @returns {string[]} Additional keywords.
   */
  get keys() {
    return this._keywords;
  }

  set language(code) {
    this._language = code;
  }

  /**
   * Set tokenized version of additional search keys.
   * @param {string[]} words - The additional search keys to store.
   */
  set keysTokens(words) {
    this._keysTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of additional search keys.
   * @returns {string[]} Tokenized additional search keys.
   */
  get keysTokens() {
    return [...this._keysTokens];
  }

  /**
   * Get analysis locale settings.
   * @returns {?string} - ISO 3166 Alpha-2 country code.
   */
  get locale() {
    return this._locale;
  }

  set results(object) {
    this._results.push(object);
  }

  get results() {
    return this._results;
  }

  /**
   * Get all the search parameters.
   * @returns {object} The search parameters.
   */
  get parameters() {
    return {
      query: this._query,
      queryTokens: this.queryTokens,
      keywords: this._keywords,
      keysTokens: this.keysTokens,
      locale: this._locale,
      language: this._language,
      inclusions: this.inclusions,
      incsTokens: this.incsTokens,
      exclusions: this.exclusions,
      excsTokens: this.excsTokens,
    };
  }

  /**
   * Get main search key.
   * @returns {string} The main search key.
   */
  get query() {
    return this._query;
  }

  get period() {
    return this._period;
  }

  /**
   * Set tokenized version of the main search key.
   * @param {string[]} words - Query tokens.
   */
  set queryTokens(words) {
    this._queryTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get main search query tokens.
   * @returns {string[]} - Main search key tokens.
   */
  get queryTokens() {
    return [...this._queryTokens];
  }

  /**
   * Detect language of the provided textual chunk.
   * @param {String|String[]} chunk - Some text or tokens to get language of.
   * @param {string} hint - ISO 3166 Alpha-2 country code
   * @returns {Promise}
   */
  static detectQueryLang(chunk, hint) {
    return new Promise(resolve => {
      // stringify incoming chunk
      const text = (_.isArray(chunk))
        ? chunk.join(' ')
        : chunk;

      // add tld hint if locale preset has been included
      const options = {};
      if (hint && hint.length && hint !== 'global') {
        options.tldHint = hint;
      }

      cld.detect(text, options, (error, result) => {
        if (result && !error) {
          // FIXME: remove array compacting when the cld languages bug gets fixed.
          const languages = _.compact(result.languages);
          resolve(languages[0].code || 'en');
        } else {
          resolve('en');
        }
      });
    });
  }

  /**
   * Extract hashtags from provided textual chunk.
   * @param {String} text - Textual chunk to be scanned.
   * @returns {String[]} Parsed hashtags.
   */
  static extractHashTags(text) {
    const regex = /(#[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]+)/ig;

    return text.match(regex);
  }

  /**
   * Tokenize incoming words and sentences.
   * @param {string|string[]} item - Some standalone word ar a set of words to be tokenized.
   * @param {string} lang - Language of the incoming item.
   * @returns {string|string[]} Stemmed tokens.
   */
  static createTokens(item, lang) {
    // languages supported by natural#PorterStemmer
    const fullySupported = ['it', 'fr', 'es', 'fa', 'pt'];
    // languages supported by natural#Stemmer
    const semiSupported = ['ja', 'pl'];
    let tokenizer;

    if (lang in fullySupported) {
      tokenizer = natural[`PorterStemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang in semiSupported) {
      tokenizer = natural[`Stemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang === 'en') {
      tokenizer = natural.PorterStemmer.tokenizeAndStem;
    } else if (lang === 'ru') {
      tokenizer = natural.PorterStemmerRu.tokenizeAndStem;
    } else if (lang === 'sv' || lang === 'nn' || lang === 'nb') {
      tokenizer = natural.PorterStemmerNo.tokenizeAndStem;
    } else if (lang === 'uk') {
      tokenizer = ukrStem;
    } else if (lang === 'cs') {
      tokenizer = czechStem;
    } else {
      tokenizer = _.words;
    }

    const text = (typeof item === 'string')
      ? item
      : item.join(' ');

    const results = tokenizer(text);
    logger.debug(`@CREATE-TOKENS :: DONE [${lang}] -> ${results}`);
    return results;
  }

  /**
   * Set up search parameters by defining language of search keys and tokenize them.
   * @param {string[]} exclusions - The minus-words.
   * @param {string[]} inclusions - The plus-words.
   * @param {string[]} keys - Secondary search keys.
   * @param {string} query - Primary search key
   * @returns {Promise}
   * @private
   */
  _processParameters(query, keys, inclusions, exclusions) {
    const compoundQuery = _.union(keys, inclusions, exclusions);

    return GPlusSearch.detectQueryLang(compoundQuery, this._locale)
      .then((languageCode) => {
        // set up search parameters
        this.keysTokens = GPlusSearch.createTokens(keys, this._language);
        this.incsTokens = GPlusSearch.createTokens(inclusions, this._language);
        this.excsTokens = GPlusSearch.createTokens(exclusions, this._language);
        this.queryTokens = GPlusSearch.createTokens(query, this._language);
        this.language = languageCode;
      });
  }

  /**
   * Processes post textual contents.
   * @param {Object} item - Set of properties representing Google+ post.
   * @param {number} index - Post's position within Google+ API search results.
   * @returns {Promise.<Post>} Processed post.
   * @private
   *
   * @param item.actor - Information about post's creator.
   * @param item.actor.displayName - Post author's alias,
   * @param item.object - Post contents.
   * @param item.verb - Post type.
   * @param item.published - Post's publication date.
   * @param item.resharers - # of post sharers.
   * @param item.plusoners - # of post likes.
   * @param item.url - Post's URL.
   *
   * @property totalItems - total count of some Google+ API property.
   * @property fullImage - URL to the resolution enclosed image.
   */
  _processPostContent(item, index) {
    const post = new Post();
    let textString = item.object.content || '';

    post.author = item.actor.displayName;
    post.channelTitle = item.actor.displayName;
    post.authorUrl = item.actor.url;
    post.postType = 'text';
    post.link = item.url;
    post.date = item.published;
    post.text = item.object.content || '';
    post.channel = item.object.url.split('/posts/')[0];

    post.index = index;
    post.reposts = item.object.resharers.totalItems || 0;

    post.followers = 0;

    post.likes = item.object.plusoners.totalItems || 0;

    post.comments = item.object.replies.totalItems || 0;
    post.verified = false;

    const attachments = item.object.attachments;
    if (attachments && attachments.length) {
      _.forEach(attachments, a => {
        if (a.objectType === 'article') {
          textString += `${a.content || ''}`;
          post.links = a.url;
          post.postType = 'link';

          if (a.fullImage) {
            post.pictures = a.fullImage.url;
          }
        } else if (a.objectType === 'photo' || a.objectType === 'video') {
          textString += `${a.displayName || ''} ${a.content || ''}`;
          post.pictures = a.url;
          post.postType = a.objectType;
        }
      });
    }

    post.hashtags = GPlusSearch.extractHashTags(textString);

    return Promise.resolve(post);
  }

  /**
   * Scan if some set of keywords is indeed present within Google+ post.
   * @param {string} body - Google+ post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @param {boolean} inTitle - Reveals if the scan is to be perform within post's title.
   * @param {boolean} partial - Reveals if the scan is being performed on tokenized keywords.
   * @returns {Promise}
   * @private
   */
  _scanHits(body, items, tokenized, type, inTitle, partial = false) {
    return new Promise(resolve => {
      const target = body.toLowerCase();
      let result;
      let stake;

      if (type === 'query') {
        // check if there's any occurrence of the `query`-items within webpage
        result = _.every(items, i => target.indexOf(i) !== -1);
        stake = (inTitle) ? 0.4 : 0.15;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * +result
        );
      } else if (type === 'keys') {
        // count how many keywords of `keys` type are present within the webpage
        result = _.reduce(items, (counter, i) => {
          return (target.indexOf(i) !== -1)
            ? counter++
            : counter;
        }, 0);
        stake = (inTitle) ? 0.35 : 0.1;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * (+result / items.length)
        );
      } else if (type === 'inclusions') {
        // check if all the `inclusions` are contained within the webpage
        result = _.every(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      } else {
        // check if none of the `exclusions` are contained within the webpage
        result = _.some(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      }
    });
  }

  /**
   * Search for analysis-defining keywords within the Google+ post.
   * @param {string} title - Google+ post's title (i.e. author's name)
   * @param {string} body - Google+ post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @returns {Promise}
   * @private
   */
  _scanPost(title, body, items, tokenized, type) {
    return Promise.all([
      this._scanHits(title, items, tokenized, type, true),
      this._scanHits(body, items, tokenized, type, false),
    ]);
  }

  /**
   * Evaluate to what extent does some page comply to search request.
   * @returns {Promise}
   * @private
   */
  _evaluateAccuracy(post, title, parameters) {
    const body = post.fullText.toLocaleLowerCase();
    const prettyTitle = title.toLocaleLowerCase();

    return Promise.all([
      this._scanPost(prettyTitle, body, [parameters.query], parameters.queryTokens, 'query'),
      this._scanPost(prettyTitle, body, parameters.keys, parameters.keysTokens, 'keys'),
      this._scanPost(prettyTitle, body, this.inclusions, this.incsTokens, 'inclusions'),
      this._scanPost(prettyTitle, body, this.exclusions, this.excsTokens, 'exclusions'),
    ]).then(data => {
      const [,, inclusions, exclusions] = data;
      const marks = _.flatten(_.slice(data, 0, 2));
      const rate = _.reduce(marks, (m, value) => m += value, 0);

      const proceed = (
        _.every(inclusions, i => i === true) && _.every(exclusions, i => i === false)
      );

      return Promise.resolve({ rate, proceed });
    });
  }

  /**
   * Evaluate Google+ post's potential influence.
   * @param {Object} post - Set of properties representing Google+ post.
   * @param {Object} info - Set of properties representing Google+ channel.
   * @returns {Promise.<number>} Calculated social potential rate.
   * @private
   */
  static _evaluatePotency(post, info) {
    const timeDiff = _.now() - new Date(post._date).getTime();
    const timePassed = Math.round(new moment.duration(timeDiff).asHours());
    const likes = (post._info.likes + 1) / 1000;
    const reposts = post._info.reposts / 1000;
    const members = 1;
    const speed = info.updateRate;
    const position = post._position;
    const likesPerMonth = info.likesPerMonth;
    let rate = 0;

    if (timePassed * 0.0003366 < 0.8) {
      const currentArea = ((0.04 + (timePassed * 0.0003366)) *
        (likesPerMonth / members * 0.1 * 25) * (likes + 4 * reposts)) *
        Math.pow(members, 2) + (0.96 - (timePassed * 0.0003366)) *
        (1 / ((speed + 1) * position * position)) * members;

      const idealArea = (likes + 4 * reposts) * (likesPerMonth / members * 0.1 * 25) *
        Math.pow(members, 2);

      const socialPotential = Math.round((1 - (currentArea / idealArea)) * 100);

      rate = socialPotential < 0 ? 100 : socialPotential;
    }

    return Promise.resolve(rate);
  }

  /**
   * Calculate secondary post rates.
   * @param {Object} post - Set of properties representing Google+ post.
   * @param {string} title - Post's title.
   * @param {Object} info - Set of properties representing Google+ channel.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _evaluatePostRates(post, title, info, parameters) {
    return Promise.all([
      GPlusSearch.detectQueryLang(post.fullText, parameters.locale),
      this._evaluateAccuracy(post, title, parameters),
      GPlusSearch._evaluatePotency(post, info),
    ])
      .then(data => {
        const [lang, accuracy, potency] = data;
        const proceed = accuracy.proceed;
        post.language = lang;
        post.accuracy = accuracy.rate;
        post.potency = potency;

        if (proceed && post.accuracy > 10 && post.potency > 10) {
          this.results = post.info;
        }
      })
      .catch(error => {
        logger.error(`@POST-ANALYSIS :: post rates processing failure -> ${error}`);
        logger.error(error.stack);
        return null;
      });
  }

  /**
   * Perform Google+ posts processing.
   * @param {Object} item - Set of properties representing Google+ post.
   * @param {number} index - Post's position within Google+ API search results.
   * @param {Object} info - Set of properties representing Google+ channel.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise.<Promise>}
   * @private
   */
  _evaluatePost(item, index, info, parameters) {
    const title = `${item.title || ''}`;

    return this._processPostContent(item, index)
      .then(post => this._evaluatePostRates(post, title, info, parameters));
  }

  /**
   * Perform parallel Google+ posts processing.
   * @param {Object[]} posts - Set of Google+ channel feeds.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _processPosts(posts, parameters) {
    const dateRange = _.map(posts, (post) => {
      const time = new Date(post.published);
      return `${time.getFullYear()}-${time.getMonth()}-${time.getDate()}`;
    });

    const likesPerMonth = _.reduce(posts, (acc, post) => {
      const timeDiff = _.now() - new Date(post.published).getTime();
      if (timeDiff > 2592e6) {
        acc += (post.likes && post.likes.summary && post.likes.summary.total_count)
          ? post.likes.summary.total_count
          : 0;
      }

      return acc;
    }, 0);

    const updateRate = _.uniq(dateRange).length / posts.length;
    const info = { likesPerMonth, updateRate };

    const tasks = _.map(posts, (p, index) => this._evaluatePost(p, index, info, parameters));

    return Promise.all(tasks);
  }

  search() {
    return this._processParameters(this._query, this._keywords, this._inclusions, this._exclusions)
      .then(() => gp.search(this.query))
      .then(data => this._processPosts(data.response, this.parameters))
      .then(() => {
        if (this.results && !this.results.length) {
          logger.info(`@G+#search :: [${this.query} --${this.period}] -> **NO** leads retrieved`);
          throw new Error('No leads retrieved');
        }

        logger.info(`@G+#search :: [${this.query} --${this.period}] -> ${this.results.length} ` +
          'leads retrieved.');

        return this.results;
      })
      .catch(error => {
        logger.error(`@G+#search :: ALERT -> ${error.message || error}`);

        if (error && error.message !== 'No leads retrieved') {
          logger.error(error.stack);
          return { error: { code: 1, message: error.message } };
        }

        return { error: { code: 0, message: 'No leads retrieved' } };
      });
  }
}

module.exports = GPlusSearch;
