/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
/* eslint new-cap: 0 */
'use strict';

const _ = require('lodash');
const cld = require('cld');
const Country = require('@startup-erector/traveller');
const czechStem = require('czech-stemmer');
const moment = require('moment');
const natural = require('natural');
const ukrStem = require('ukrstemmer');
const winston = require('winston');

const EnhancedSet = require('./enhanced-set');
const FBClient = require('./facebook-api');
const Post = require('./post');
const tools = require('./tools');

const fb = new FBClient(process.env.FACEBOOK_TOKEN);
// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

class FacebookSearch {
  /**
   * @param parameters - The set of parameters defining an instance of analysis.
   * @param {string[]} parameters.keys - Secondary search keys.
   * @param {!string} parameters.query - The main search key.
   * @param {?string} [parameters.locale=null] - ISO 3166 Alpha-2 country code.
   * @param {string[]} [parameters.inclusions=[]] - The set of plus-words.
   * @param {string[]} [parameters.exclusions=[]] - The set of minus-words.
   */
  constructor(parameters) {
    const { query, locale = 'global', period = 'y', keys = [], settings = {} } = parameters;
    const { include = [], exclude = [] } = settings;

    /** Converts either items or set of items to lower case */
    function lowerCase(item) {
      if (typeof item === 'string') {
        return item.toLocaleLowerCase();
      }
      return _.map(item, element => element.toLocaleLowerCase());
    }

    this._exclusions = lowerCase(exclude);
    this._excsTokens = new EnhancedSet();
    this._inclusions = lowerCase(include);
    this._incsTokens = new EnhancedSet();
    this._keywords = lowerCase(keys);
    this._keysTokens = new EnhancedSet();
    this._language = null;
    this._locale = locale;
    this._period = period;
    this._query = lowerCase(query);
    this._queryTokens = new EnhancedSet();
    this._results = [];
  }

  /**
   * Get analysis minus-words.
   * @returns {String[]} The minus-words.
   */
  get exclusions() {
    return this._exclusions;
  }

  /**
   * Set tokenized version of minus-words.
   * @param {string[]} words - The minus-words to store.
   */
  set excsTokens(words) {
    this._excsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of minus-words.
   * @returns {string[]} Tokenized minus-words.
   */
  get excsTokens() {
    return [...this._excsTokens];
  }

  /**
   * Get analysis plus-words.
   * @returns {String[]} The plus-words.
   */
  get inclusions() {
    return this._inclusions;
  }

  /**
   * Set tokenized version of plus-words.
   * @param {string[]} words - The plus-words to store.
   */
  set incsTokens(words) {
    this._incsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of plus-words.
   * @returns {string[]} Tokenized plus-words.
   */
  get incsTokens() {
    return [...this._incsTokens];
  }

  /**
   * Get analysis additional keywords.
   * @returns {string[]} Additional keywords.
   */
  get keys() {
    return this._keywords;
  }

  set language(code) {
    this._language = code;
  }

  /**
   * Set tokenized version of additional search keys.
   * @param {string[]} words - The additional search keys to store.
   */
  set keysTokens(words) {
    this._keysTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of additional search keys.
   * @returns {string[]} Tokenized additional search keys.
   */
  get keysTokens() {
    return [...this._keysTokens];
  }

  /**
   * Get analysis locale settings.
   * @returns {?string} - ISO 3166 Alpha-2 country code.
   */
  get locale() {
    return this._locale;
  }

  set results(object) {
    this._results.push(object);
  }

  get results() {
    return this._results;
  }

  /**
   * Get all the search parameters.
   * @returns {object} The search parameters.
   */
  get parameters() {
    return {
      query: this._query,
      queryTokens: this.queryTokens,
      keywords: this._keywords,
      keysTokens: this.keysTokens,
      locale: this._locale,
      language: this._language,
      inclusions: this.inclusions,
      incsTokens: this.incsTokens,
      exclusions: this.exclusions,
      excsTokens: this.excsTokens,
    };
  }

  /**
   * Ger search timeframe
   * @returns {String} The search timeframe.
   */
  get period() {
    return this._period;
  }

  /**
   * Get main search key.
   * @returns {string} The main search key.
   */
  get query() {
    return this._query;
  }

  /**
   * Set tokenized version of the main search key.
   * @param {string[]} words - Query tokens.
   */
  set queryTokens(words) {
    this._queryTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get main search query tokens.
   * @returns {string[]} - Main search key tokens.
   */
  get queryTokens() {
    return [...this._queryTokens];
  }

  /**
   * Detect language of the provided textual chunk.
   * @param {String|String[]} chunk - Some text or tokens to get language of.
   * @param {string} hint - ISO 3166 Alpha-2 country code
   * @returns {Promise}
   */
  static detectQueryLang(chunk, hint) {
    return new Promise(resolve => {
      // stringify incoming chunk
      const text = (_.isArray(chunk))
        ? chunk.join(' ')
        : chunk;

      // add tld hint if locale preset has been included
      const options = {};
      if (hint && hint.length && hint !== 'global') {
        options.tldHint = hint;
      }

      cld.detect(text, options, (error, result) => {
        if (result && !error) {
          // FIXME: remove array compacting when the cld languages bug gets fixed.
          const languages = _.compact(result.languages);
          resolve(languages[0].code || 'en');
        } else {
          resolve('en');
        }
      });
    });
  }

  /**
   * Tokenize incoming words and sentences.
   * @param {string|string[]} item - Some standalone word ar a set of words to be tokenized.
   * @param {string} lang - Language of the incoming item.
   * @returns {string|string[]} Stemmed tokens.
   */
  static createTokens(item, lang) {
    // languages supported by natural#PorterStemmer
    const fullySupported = ['it', 'fr', 'es', 'fa', 'pt'];
    // languages supported by natural#Stemmer
    const semiSupported = ['ja', 'pl'];
    let tokenizer;

    if (lang in fullySupported) {
      tokenizer = natural[`PorterStemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang in semiSupported) {
      tokenizer = natural[`Stemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang === 'en') {
      tokenizer = natural.PorterStemmer.tokenizeAndStem;
    } else if (lang === 'ru') {
      tokenizer = natural.PorterStemmerRu.tokenizeAndStem;
    } else if (lang === 'sv' || lang === 'nn' || lang === 'nb') {
      tokenizer = natural.PorterStemmerNo.tokenizeAndStem;
    } else if (lang === 'uk') {
      tokenizer = ukrStem;
    } else if (lang === 'cs') {
      tokenizer = czechStem;
    } else {
      tokenizer = _.words;
    }

    const text = (typeof item === 'string')
      ? item
      : item.join(' ');

    const results = tokenizer(text);
    logger.debug(`@CREATE-TOKENS :: DONE [${lang}] -> ${results}`);
    return results;
  }

  /**
   * Extract hashtags from provided textual chunk.
   * @param {String} text - Textual chunk to be scanned.
   * @returns {String[]} Parsed hashtags.
   */
  static extractHashTags(text) {
    const regex = /(#[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9_]+)/ig;

    return text.match(regex);
  }

  /**
   * Set up search parameters by defining language of search keys and tokenize them.
   * @param {string[]} exclusions - The minus-words.
   * @param {string[]} inclusions - The plus-words.
   * @param {string[]} keys - Secondary search keys.
   * @param {string} query - Primary search key
   * @returns {Promise}
   * @private
   */
  _processParameters(query, keys, inclusions, exclusions) {
    const compoundQuery = _.union(keys, inclusions, exclusions);

    return FacebookSearch.detectQueryLang(compoundQuery, this._locale)
      .then((languageCode) => {
        // set up search parameters
        this.keysTokens = FacebookSearch.createTokens(keys, this._language);
        this.incsTokens = FacebookSearch.createTokens(inclusions, this._language);
        this.excsTokens = FacebookSearch.createTokens(exclusions, this._language);
        this.queryTokens = FacebookSearch.createTokens(query, this._language);
        this.language = languageCode;
      });
  }

  /**
   * Filter dead or non-active Facebook channels.
   * @param {Object[]} channels - Channels to be filtered.
   * @param {Object} parameters - Leads search parameters.
   * @returns {Promise.<Object[]>} Active and relevant channels
   * @private
   *
   * @property updated_time - Facebook post's time of last update.
   * @property created_time - Facebook post's time of creation.
   * @property general_info - Some general info provided by Facebook channel.
   * @property can_post - Reveals if it's possible to post to the channel.
   * @property privacy - Reveals if the channel is public.
   * @property feed - Facebook channel's feed
   * @property engagement - Reveals # of Facebook page followers.
   * @property total_count - Reveals quantity of some Facebook engagement property.
   */
  _filterChannels(channels, parameters) {
    function lastActivity(posts, type) {
      const SCAN_WINDOW = _.now() - ((parameters.period === 'y') ? 8e9 : 1296e5);
      const date = _.reduce(posts, (latest, post) => {
        if (type === 'group') {
          return (post.updated_time > latest)
            ? post.updated_time
            : latest;
        }

        return (post.created_time > latest)
          ? post.created_time
          : latest;
      }, 0);

      return (date < SCAN_WINDOW);
    }

    const results = _.filter(channels, c => {
      const target = (c.type === 'page')
        ? `${c.name || ''} ${c.about || ''} ${c.description || ''} ${c.general_info || ''}`
        : `${c.name || ''} ${c.description || ''}`;

      const byText = tools.find(target, parameters.keysTokens);
      const byPrivacy = c.can_post || c.privacy === 'OPEN';

      let byActivity = false;
      let hasPosts = false;

      if (c.feed && c.feed.data && c.feed.data.length) {
        hasPosts = !!c.feed.data.length;
      }

      if (hasPosts) {
        byActivity = lastActivity(c.feed.data);
      }

      const hasFollowers = (c.type === 'page')
        ? c.engagement.count
        : c.members.summary.total_count;

      return byText && byActivity && byPrivacy && hasPosts && hasFollowers;
    });

    return Promise.resolve(results);
  }

  /**
   * Processes post textual contents.
   * @param {Object} item - Set of properties representing Facebook post.
   * @param {Object} channel - Set of properties representing Facebook channel.
   * @returns {Promise.<Post>} Processed post.
   * @private
   *
   * @property place - Facebook post's property representing related location.
   * @property city - Represents related city.
   * @property shares - Facebook post reposts.
   * @property from.id - Facebook ID of the post publisher.
   * @property is_verified - Reveals if the source channel is officially verified.
   */
  static _analyzePostContents(item, channel) {
    const post = new Post();
    const textString = `${item.message || ''} ${item.caption || ''} ${item.description || ''}`;

    post.author = item.from.name;
    post.authorUrl = `https://facebook.com/${item.from.id}`;
    post.postType = item.type;
    post.link = (item.actions && item.actions.length) ? item.actions[0].link : '';
    post.date = item.created_time;
    post.text = textString;
    post.channelTitle = channel.name;
    post.channel = `https://facebook.com/${channel.username || channel.id}`;
    post.location = (item.place && item.place.location && item.place.name)
      ? `${item.place.name || ''} ${item.place.location.city || ''} ` +
    `${item.place.location.country}`
      : null;

    post.index = item.index;
    post.reposts = (item.shares) ? item.shares.count : 0;
    post.updateRate = channel.updateRate;
    post.followers = (channel.type === 'page')
      ? channel.engagement.count
      : channel.members.summary.total_count;

    post.likes = (item.likes && item.likes.summary)
      ? item.likes.summary.total_count
      : 0;

    post.comments = (item.comments && item.comments.summary)
      ? item.comments.summary.total_count
      : 0;
    post.verified = channel.is_verified || false;

    post.hashtags = FacebookSearch.extractHashTags(textString);

    if (item.pictures) {
      post.pictures = item.pictures;
    }

    if (item.links) {
      post.links = item.link;
    }

    if (item.type === 'video') {
      post.videos = item.link;
    }

    post.remove = (item.type === 'event' ||
    !item.actions || !item.actions.length);

    return Promise.resolve(post);
  }

  /**
   * Scan if some set of keywords is indeed present within Facebook post.
   * @param {string} body - Facebook post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @param {boolean} inTitle - Reveals if the scan is to be perform within post's title.
   * @param {boolean} partial - Reveals if the scan is being performed on tokenized keywords.
   * @returns {Promise}
   * @private
   */
  _scanHits(body, items, tokenized, type, inTitle, partial = false) {
    return new Promise(resolve => {
      const target = body.toLowerCase();
      let result;
      let stake;

      if (type === 'query') {
        // check if there's any occurrence of the `query`-items within webpage
        result = _.every(items, i => target.indexOf(i) !== -1);
        stake = (inTitle) ? 0.4 : 0.15;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * +result
        );
      } else if (type === 'keys') {
        // count how many keywords of `keys` type are present within the webpage
        result = _.reduce(items, (counter, i) => {
          return (target.indexOf(i) !== -1)
            ? counter++
            : counter;
        }, 0);
        stake = (inTitle) ? 0.35 : 0.1;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * (+result / items.length)
        );
      } else if (type === 'inclusions') {
        // check if all the `inclusions` are contained within the webpage
        result = _.every(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      } else {
        // check if none of the `exclusions` are contained within the webpage
        result = _.some(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      }
    });
  }

  /**
   * Search for analysis-defining keywords within the Facebook post.
   * @param {string} title - Facebook post's title (i.e. author's name)
   * @param {string} body - Facebook post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @returns {Promise}
   * @private
   */
  _scanPost(title, body, items, tokenized, type) {
    return Promise.all([
      this._scanHits(title, items, tokenized, type, true),
      this._scanHits(body, items, tokenized, type, false),
    ]);
  }

  /**
   * Evaluate to what extent does some page comply to search request.
   * @returns {Promise}
   * @private
   */
  _evaluateAccuracy(post, channel, parameters) {
    const title = `${channel.name || ''} ${channel.about || ''} ${channel.description || ''}`
      .toLocaleLowerCase();

    const body = post.fullText.toLocaleLowerCase();

    return Promise.all([
      this._scanPost(title, body, [parameters.query], parameters.queryTokens, 'query'),
      this._scanPost(title, body, parameters.keys, parameters.keysTokens, 'keys'),
      this._scanPost(title, body, this.inclusions, this.incsTokens, 'inclusions'),
      this._scanPost(title, body, this.exclusions, this.excsTokens, 'exclusions'),
    ]).then(data => {
      const [,, inclusions, exclusions] = data;
      const marks = _.flatten(_.slice(data, 0, 2));
      const rate = _.reduce(marks, (m, value) => m += value, 0);

      const proceed = (
        _.every(inclusions, i => i === true) && _.every(exclusions, i => i === false)
      );

      return Promise.resolve({ rate, proceed });
    });
  }

  /**
   * Evaluate Facebook post's potential influence.
   * @param {Object} post - Set of properties representing Facebook post.
   * @param {Object} channel - Set of properties representing Facebook channel.
   * @returns {Promise.<number>} Calculated social potential rate.
   * @private
   */
  static _evaluatePotency(post, channel) {
    const timeDiff = _.now() - new Date(post._date).getTime();
    const timePassed = Math.round(new moment.duration(timeDiff).asHours());
    const likes = (post._info.likes + 1) / 1000;
    const reposts = post._info.reposts / 1000;
    const members = post._followers / 1000;
    const speed = post._updateRate;
    const position = post._position;
    const likesPerMonth = channel.likesPerMonth / 1000;
    let rate = 0;

    if (timePassed * 0.0003366 < 0.8) {
      const currentArea = ((0.04 + (timePassed * 0.0003366)) *
        (likesPerMonth / members * 0.1 * 25) * (likes + 4 * reposts)) *
        Math.pow(members, 2) + (0.96 - (timePassed * 0.0003366)) *
        (1 / ((speed + 1) * position * position)) * members;

      const idealArea = (likes + 4 * reposts) * (likesPerMonth / members * 0.1 * 25) *
        Math.pow(members, 2);

      const socialPotential = Math.round((1 - (currentArea / idealArea)) * 100);

      rate = socialPotential < 0 ? 100 : socialPotential;
    }

    return Promise.resolve(rate);
  }

  /**
   * Calculate secondary post rates.
   * @param {Object} post - Set of properties representing Facebook post.
   * @param {Object} channel - Set of properties representing Facebook channel.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _evaluatePostRates(post, channel, parameters) {
    return Promise.all([
      FacebookSearch.detectQueryLang(post.text, parameters.locale),
      this._evaluateAccuracy(post, channel, parameters),
      FacebookSearch._evaluatePotency(post, channel),
    ])
      .then(data => {
        const [lang, accuracy, potency] = data;
        const proceed = accuracy.proceed;

        let understandable = parameters.locale === 'global';

        if (parameters.locale !== 'global') {
          const country = new Country(parameters.locale);
          const languages = country.languageCodes;

          if (parameters.locale === 'ua') {
            understandable = (lang === 'uk' || lang === 'ru');
          } else {
            understandable = languages.indexOf(lang) !== -1;
          }
        }

        post.accuracy = accuracy.rate;
        post.language = lang;
        post.potency = potency;

        if (proceed && post.accuracy > 15 && post.potency > 10 && !post._remove && understandable) {
          this.results = post.info;
        }
      })
      .catch(error => {
        logger.warn(`@POST-ANALYSIS :: post rates processing failure -> ${error}`);
        return null;
      });
  }

  /**
   * Perform Facebook posts processing.
   * @param {Object} object - Set of properties representing Facebook post.
   * @param {Object} channel - Set of properties representing Facebook channel.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise.<Promise>}
   * @private
   */
  _processPost(object, channel, parameters) {
    return FacebookSearch._analyzePostContents(object, channel)
      .then(post => this._evaluatePostRates(post, channel, parameters));
  }

  /**
   * Calculate how many posts and likes are generated within this channel per month.
   * @param {Object} feed - Facebook channel posts feed.
   * @returns {Promise.<{updateRate: number, likesPerMonth: number}>}
   * @private
   */
  static _getChannelUpdateRate(feed) {
    const dateRange = _.map(feed, (post) => {
      const time = new Date(post.created_time);
      return `${time.getFullYear()}-${time.getMonth()}-${time.getDate()}`;
    });

    const likesPerMonth = _.reduce(feed, (acc, post) => {
      const timeDiff = _.now() - new Date(post.created_time).getTime();
      if (timeDiff > 2592e6) {
        acc += (post.likes && post.likes.summary && post.likes.summary.total_count)
          ? post.likes.summary.total_count
          : 0;
      }

      return acc;
    }, 0);

    const updateRate = _.uniq(dateRange).length / feed.length;

    return Promise.resolve({ updateRate, likesPerMonth });
  }

  /**
   * Perform parallel Facebook posts processing.
   * @param {Object[]} objects - Set of Facebook channel feeds.
   * @param {Object[]} objects.response - Facebook client response field containing actual response.
   * @param {Object} channel - Facebook channel that gets scanned.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _processFeed(objects, channel, parameters) {
    return FacebookSearch._getChannelUpdateRate(objects.response)
      .then(data => {
        channel.updateRate = data.updateRate;
        channel.likesPerMonth = data.likesPerMonth;
        const tasks = _.map(objects.response, p => this._processPost(p, channel, parameters));

        return Promise.all(tasks);
      });
  }

  /**
   * Perform parallel Facebook posts processing.
   * @param {Object[]} objects - Set of Facebook channel feeds.
   * @param {Object[]} objects.response - Facebook client response field containing actual response.
   * @param {Object} channel - Facebook channel that gets scanned.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _analyzeChannel(channel, parameters) {
    return fb.getFeed(channel.id, this._period)
      .then(data => {
        return this._processFeed(data, channel, parameters);
      });
  }

  /**
   * Retrieve potential leads out of Facebook channel feeds.
   * @param {*|Object[]} channels - Facebook channels to be scanned for leads.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise.<Object[]>} Retrieved leads.
   * @private
   */
  _getLeads(channels, parameters) {
    const tasks = _.map(channels, c => this._analyzeChannel(c, parameters));

    return Promise.all(tasks);
  }

  /**
   * Retrieve thematic-relevant Facebook channels.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _getChannels(parameters) {
    return fb.thematicChannelsSearch({
      searchKey: parameters.query,
      searchKeyLang: parameters.language,
      market: parameters.locale,
      limit: 200,
    })
      .then(channels => this._filterChannels(channels, parameters));
  }

  /**
   * Perform global leads retrieval
   * @returns {Promise}
   */
  search() {
    return this._processParameters(this._query, this._keywords, this._inclusions, this._exclusions)
      .then(() => this._getChannels(this.parameters))
      .then(channels => this._getLeads(channels, this.parameters))
      .then(() => {
        if (this.results && !this.results.length) {
          logger.info(`@FB#search :: [${this.query} --${this.period}] -> **NO** leads retrieved`);
          throw new Error('No leads retrieved');
        }

        logger.info(`@FB#search :: [${this.query} --${this.period}] -> ${this.results.length} ` +
          'leads retrieved.');

        const leads = _.uniqBy(this.results, 'link');
        console.log('TOTAL LEADS FOUND :: ', this.results.length);
        console.log('UNIQUE LEADS FOUND :: ', leads.length);

        return this.results;
      })
      .catch(error => {
        logger.error(`@FB#search :: ALERT -> ${error.message || error}`);

        if (error && error.message !== 'No leads retrieved') {
          logger.error(error.stack);
          return { error: { code: 1, message: error.message } };
        }

        return { error: { code: 0, message: 'No leads retrieved' } };
      });
  }
}

module.exports = FacebookSearch;
