/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const _ = require('lodash');
const querystring = require('querystring');
const request = require('request');
const url = require('url');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

class GooglePlus {
  constructor(accessToken) {
    this._token = accessToken;
    this.DAILY_SCAN_WINDOW = 1296e5; // 36h time frame
    this.FULL_SCAN_WINDOW = 8e9; // 3 months time frame
  }

  /**
   * Convert Google+ API response from string to JSON.
   * @param {string} responseBody - Google+ API response
   * @returns {Promise.<Object>} The response JSON.
   * @private
   */
  _parseAPIResponse(responseBody) {
    return new Promise((resolve, reject) => {
      try {
        resolve(JSON.parse(responseBody));
      } catch (parseError) {
        logger.error(`@G+ :: failed to parse G+ API response -> ${parseError}.`);

        reject(new Error('Unexpected G+ API response'));
      }
    })
      .then(responseObject => {
        if (responseObject.error) {
          throw new Error(responseObject.error.message);
        } else {
          return responseObject;
        }
      });
  }

  /**
   * Performs atomic Google+ requests
   * @private
   * @param {string} link - URL of API endpoint
   *
   * @returns {Promise.<Object>} containing parsed Google+ request
   * @rejects if either request or response parsing fails
   */
  _performAPIRequest(link) {
    return new Promise((resolve, reject) => {
      request({
        url: link,
        method: 'GET',
      }, (error, response) => {
        if (response && response.body && !error) {
          this._parseAPIResponse(response.body)
            .then(dataObject => resolve(dataObject))
            .catch(errorObject => reject(errorObject));
        } else {
          logger.error(
            '@G+ :: API request failed ->' +
              `${(response && response.body) ? response.body : error.message}`
          );

          reject(new Error('Failed to establish connection with G+ API'));
        }
      });
    });
  }

  /**
   * Performs successive Google+ API request and response processing.
   * @access private
   * @param {Object} parameters - Collection of request criteria.
   * @param {string} parameters.url - API endpoint url to call.
   * @param {string} parameters.query - The search query.
   * @param {string} [parameters.locale] - ISO 3166 alpha 2 country code.
   * @param {string} parameters.type - The entity type to retrieve.
   * @param {string} [parameters.target] - Target language of the search query.
   * @param {number} [tries] - # of tries left.
   * @param {number} [pages] - Defines how many pages of data to retrieve.
   * @param {Object[]} [results] - Accumulates results.
   *
   * @property nextPageToken - paging token returned by Google+ API.
   *
   * @returns {Promise.<Object>} containing retrieved data.
   * @rejects if any error occurs while data extraction.
   */
  _apiRequest(parameters, pages = 2, tries = 4, results = []) {
    const { query, market = 'global', type } = parameters;

    return new Promise((resolve) => {
      this._performAPIRequest(parameters.url)
        .then(response => {
          const accumulator = _.concat(results, response.items);

          // jump up to the next page of results if it's not prohibited by `pages` parameter
          if (response.nextPageToken && (pages > 0 || pages === Infinity)) {
            parameters.url = `${parameters.url.split('&pageToken=')[0]}` +
              `&pageToken=${encodeURI(response.nextPageToken)}`;

            resolve(this._apiRequest(parameters, tries, --pages, accumulator));
          } else {
            if (accumulator && accumulator.length) {
              logger.debug(
                `@G+#apiRequest :: <"${query}" --${market}> -> ` +
                `${accumulator.length} ${accumulator.length % 10 === 1 ? type : `${type}s`}` +
                ' retrieved.'
              );
            }

            resolve({ query, market, type, response: accumulator });
          }
        })
        .catch(error => {
          // if some weird FB server-related stuff occurred, try 3 more times
          if (error.code && error.code.toString()[0] === '5' && tries >= 3) {
            logger.error(
              `@G+#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Repeating request... Attempt: (${4 - (tries - 1)}/4)`
            );

            resolve(this._apiRequest(parameters, pages, --tries));
          } else if (error.code && error.code.toString()[0] === '5' && tries < 3 && tries > 0) {
            logger.error(
              `@G+#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Retrying... Attempt: (${4 - (tries - 1)}/4)`
            );

            resolve(this._apiRequest(parameters, pages, --tries));
          } else {
            // if there's nothing can be done, resolve with results that have previously been
            // grabbed
            logger.error(
              `@G+#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `REASON: ${error.message || error}`
            );
            resolve({ query, market, type, response: results || [] });
          }
        });
    });
  }


  /**
   * Performs Google+ publications search.
   * @param {string} query - Primary search key.
   * @param {string} [market='global'] - Target country preset.
   * @param {string} [type='post'] - Google+ entity to look for.
   * @returns {Promise.<Object>} Google+ API response.
   */
  search(query, market = 'global', type = 'post') {
    const searchRequest = {
      query,                         // the search key
      maxResults: 20,                // # of result items to extract
      key: this._token,
    };

    const endpoint = {
      protocol: 'https:',
      host: 'www.googleapis.com',
      pathname: '/plus/v1/activities',
      search: querystring.stringify(searchRequest),
    };

    return this._apiRequest({ url: url.format(endpoint), query, market, type });
  }
}

module.exports = GooglePlus;
