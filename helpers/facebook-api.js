/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const _ = require('lodash');
const Country = require('@startup-erector/traveller');
const groupFields = require('../lib/fb-group-fields');
const pageFields = require('../lib/fb-page-fields');
const postFields = require('../lib/fb-post-fields');
const querystring = require('querystring');
const request = require('request');
const url = require('url');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

class Facebook {
  constructor(accessToken) {
    this._token = accessToken;
    this.DAILY_SCAN_WINDOW = 1296e5;      // 36h time frame
    this.FULL_SCAN_WINDOW = 8e9;          // 3 months time frame
  }

  /**
   * Get starting point for the full-pledged analysis.
   * @returns {string} - Full analysis start date.
   */
  get extendedStartPoint() {
    return new Date(_.now() - this.FULL_SCAN_WINDOW).toISOString();
  }

  /**
   * Get starting point for daily analysis.
   * @returns {string} - Daily analysis start date.
   */
  get dailyStartPoint() {
    return new Date(_.now() - this.DAILY_SCAN_WINDOW).toISOString();
  }

  /**
   * Convert Facebook API response from string to JSON.
   * @param {string} responseBody - Facebook API response
   * @returns {Promise.<Object>} The response JSON.
   * @private
   */
  _parseAPIResponse(responseBody) {
    return new Promise((resolve, reject) => {
      try {
        resolve(JSON.parse(responseBody));
      } catch (parseError) {
        logger.error(`@FB :: failed to parse FB API response -> ${parseError}.`);

        reject({ code: 520, message: 'Unexpected Facebook API response' });
      }
    });
  }

  /**
   * Performs atomic Facebook API requests.
   * @private
   * @param {string} link - URL of API endpoint.
   *
   * @returns {Promise.<Object>} containing parsed Facebook request.
   * @rejects if either request or response parsing fails.
   */
  _performAPIRequest(link) {
    return new Promise((resolve, reject) => {
      request({
        url: link,
        method: 'GET',
      }, (error, response) => {
        if (response && response.body && !error) {
          // parse Facebook response
          this._parseAPIResponse(response.body)
            .then(dataObject => resolve(dataObject))
            .catch(errorObject => reject(errorObject));
        } else {
          logger.error(
            `@FB :: API request failed -> ${(response && response.body) ? response.body : error}`
          );

          reject({ code: 500, message: 'Failed to establish connection with Facebook Graph API' });
        }
      });
    });
  }

  /**
   * Performs Facebook API responses processing.
   * @access private
   * @param {Object} response - Contains full body of Facebook Graph API response.
   *
   * @returns {Promise.<Object>} Containing retrieved data and paging info.
   * @rejects if any Facebook API error is being occurred.
   */
  _normalizeFBResponse(response) {
    return new Promise((resolve, reject) => {
      if (!response.error) {
        resolve(response);
      } else {
        const code = response.error.code;

        if (code === 1 || code === 2) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - Downtime error.`
          );

          reject({ code: 507, message: 'Facebook API downtime error' });
        } else if (code === 4 || code === 17 || code === 341) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - API throttling.`
          );

          reject({ code: 507, message: 'Facebook API throttled' });
        } else if (code === 10) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - API usage permission` +
            'has been removed.'
          );

          reject({ code: 403, message: 'Facebook API usage permission has been revoked' });
        } else if (code === 102 || code === 463 || code === 467) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - Access token has` +
            'expired.'
          );

          reject({ code: 401, message: 'Facebook API access token has expired' });
        } else if (code === 368) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - API access has been` +
            'blocked.'
          );

          reject({ code: 401, message: 'Facebook API access token has expired' });
        } else if (code === 104 || code === 190) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - Access token is ` +
            'invalid or broken.'
          );

          reject({ code: 400, message: 'Facebook API access token is broken' });
        } else if (code === 2500) {
          logger.error(
            `@FB#normalizeFBResponse :: Request failed: {${code}} - Query string ` +
            'parsing error.'
          );

          reject({ code: 400, message: 'Facebook API query string is broken' });
        } else {
          logger.error(`- @FB#normalizeFBResponse() :: Request failed: {${response.error}}.`);

          reject({ code: 507, message: 'Unknown Facebook API error' });
        }
      }
    });
  }

  /**
   * Performs successive Facebook API request and response processing.
   * @access private
   * @param {Object} parameters - Collection of request criteria.
   * @param {string} parameters.url - API endpoint url to call.
   * @param {string} parameters.query - The search query.
   * @param {string} [parameters.locale] - ISO 3166 alpha 2 country code.
   * @param {string} parameters.type - The entity type to retrieve.
   * @param {string} [parameters.target] - Target language of the search query.
   * @param {number} [tries] - # of tries left.
   * @param {number} [pages] - Defines how many pages of data to retrieve.
   * @param {Object[]} [results] - Accumulates results.
   *
   * @property paging - paging data returned by Facebook API.
   *
   * @returns {Promise.<Object>} containing retrieved data.
   * @rejects if any error occurs while data extraction.
   */
  _apiRequest(parameters, pages = 2, tries = 4, results = []) {
    const { query, market = 'global', type } = parameters;

    return new Promise((resolve) => {
      this._performAPIRequest(parameters.url)
        .then(this._normalizeFBResponse)
        .then(response => {
          const accumulator = _.concat(results, response.data);

          // jump up to the next page of results if it's not prohibited by `pages` parameter
          if (response.paging && response.paging.next && (pages > 0 || pages === Infinity)) {
            parameters.url = decodeURIComponent(response.paging.next);

            resolve(this._apiRequest(parameters, tries, --pages, accumulator));
          } else {
            if (accumulator && accumulator.length) {
              logger.debug(
                `@FB#apiRequest :: <"${query}" --${market}> -> ` +
                `${accumulator.length} ${accumulator.length % 10 === 1 ? type : `${type}s`}` +
                ' retrieved.'
              );
            }

            resolve({ query, market, type, response: accumulator });
          }
        })
        .catch(error => {
          // if some weird FB server-related stuff occurred, try 3 more times
          if (error.code && error.code.toString()[0] === '5' && tries >= 3) {
            logger.error(
              `@FB#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Repeating request... Attempt: (${4 - (tries - 1)}/4)`
            );

            resolve(this._apiRequest(parameters, pages, --tries));
          } else if (error.code && error.code.toString()[0] === '5' && tries < 3 && tries > 0) {
            logger.error(
              `@FB#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Shrinking request... Attempt: (${4 - (tries - 1)}/4)`
            );

            // shrink amount of data to be returned if Facebook API is still throttling
            parameters.url = parameters.url
              .replace('&limit=100', '&limit=15')
              .replace('.limit(10)', '.limit(3)');

            resolve(this._apiRequest(parameters, pages, --tries));
          } else {
            // if there's nothing can be done, resolve with results that have previously been
            // grabbed
            logger.error(
              `@FB#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `REASON: ${error.message || error}`
            );
            resolve({ query, market, type, response: results || [] });
          }
        });
    });
  }

  /**
   * Forms up set of data to compose Facebook API request.
   *
   * @access private
   * @param {string} query - The search key.
   * @param {string} type - Facebook entity type to look for.
   * @param {string} [market] - ISO 3166 Alpha-2 country code of the target market.
   *
   * @returns {Object} that contains fields needed to compose Facebook API request.
   */
  _formChannelSearchRequests(query, type, market = 'global') {
    // API search parameters
    const searchRequest = {
      q: query,                         // the search key
      type,                             // type of entity to be retrieved (either page or group)
      fields: _.join(                   // result items' fields to extract
        (type === 'page')
          ? pageFields
          : groupFields,
        ','
      ),
      limit: 100,                       // # of result items to be extracted
      access_token: this._token,        // access token
    };

    const endpoint = {
      protocol: 'https:',
      host: 'graph.facebook.com',
      pathname: '/v2.5/search',
      search: querystring.stringify(searchRequest),
    };

    return {
      url: url.format(endpoint),
      query,
      market,
      type,
    };
  }

  /**
   *
   * @param {string} searchParameters.searchKey - The search key.
   * @param {string} searchParameters.searchKeyLang - Detected language of the search key.
   * @param {string} searchParameters.market - Target country.
   * @param [searchParameters.limit] - max # of channels to retrieve per request.
   *
   * @returns {Promise.<Object[]>} that contains data about each end every retrieved channel.
   */
  thematicChannelsSearch(searchParameters) {
    const { searchKey, searchKeyLang = 'en', market, limit } = searchParameters;
    return new Promise((resolve, reject) => {
      // evaluate number of pages to retrieve
      const pages = _.round(limit / 100) - 1;

      // form up search queries queue
      // initialize global search queries
      const searchRequests = [
        this._apiRequest(this._formChannelSearchRequests(searchKey, 'page'), pages),
        this._apiRequest(this._formChannelSearchRequests(searchKey, 'group'), pages),
      ];

      // if there's target market provided & some country-related keys were retrieved, add a couple
      // of country-specific queries (country-name/demonym pairs) to the queue of queries for each
      // country-related key
      if (market !== 'global') {
        logger.info(`@FB#search :: [${market}] to be set.`);
        const countryKeys = new Country(market).relatedNames(searchKeyLang);

        _.forEach(countryKeys, token => {
          const compoundQuery = `${searchKey} ${token}`;

          searchRequests.push(
            this._apiRequest(
              this._formChannelSearchRequests(compoundQuery, 'page', market), pages
            )
          );

          searchRequests.push(
            this._apiRequest(
              this._formChannelSearchRequests(compoundQuery, 'group', market), pages
            )
          );
        });
      }

      // perform api requests
      Promise.all(searchRequests)
        .then(chunks => {
          const channels = _.reduce(chunks, (accumulator, chunk) => {
            const temp = _.map(chunk.response, channel => {
              channel.query = searchKey;
              channel.type = chunk.type;
              channel.market = chunk.market;
              return channel;
            });
            return _.concat(accumulator, temp);
          }, []);

          logger.info(
            `- @FB#ChannelsSearch :: successfully retrieved **${channels.length}** channels.`
          );

          resolve(channels);
        })
        .catch(error => {
          const prettyError = JSON.stringify(error, null, 2);
          logger.error(
            `- @FB#channelSearch :: Failed to retrieve relevant channels -> ${prettyError}`
          );

          reject({
            code: 500,
            message: 'Channel search failure',
            extensive: prettyError,
          });
        });
    });
  }

  /**
   * Retrieves posts from the specified Facebook channel.
   *
   * @access public
   * @param {string} id - Target Facebook channel.
   * @param {string} [since] - Defines how old should be posts to be omitted.
   *
   * @returns {Promise.<Object[]>} that contains retrieved feeds
   * @rejects if some sort of error occurs during the extraction phase
   */
  getFeed(id, since) {
    return new Promise((resolve, reject) => {
      // API search parameters
      const searchRequest = {
        since: (since === 'y')            // define since what date posts should be retrieved
          ? this.extendedStartPoint
          : this.dailyStartPoint,
        fields: _.join(postFields, ','),  // post fields to be retrieved
        limit: 100,                       // # of result items to extract
        access_token: this._token,        // access token
      };

      const endpoint = {
        protocol: 'https:',
        host: 'graph.facebook.com',
        pathname: `/v2.5/${id}/feed`,
        search: querystring.stringify(searchRequest),
      };

      this._apiRequest({ url: url.format(endpoint), query: id, type: 'post' }, Infinity)
        .then(feed => {
          // assign feed position to each retrieved post
          _.forEach(feed.response, (post, index) => post.index = index);
          resolve(feed);
        })
        .catch(error => {
          const prettyError = JSON.stringify(error, null, 2);
          logger.error(`@FB#getFeed :: Failed to retrieve posts -> ${prettyError}`);

          reject({
            code: 500,
            message: `Failed to retrieve post from channel #${id}`,
            extended: prettyError,
          });
        });
    });
  }

  /**
   * Performs parallel feeds retrieval for the specified channels.
   *
   * @param {String[]} channelIDs - List of channels to retrieve feed of.
   * @param {String} [since] - Defines how old should be posts to be omitted.
   * @param {Boolean} [pretty] - Defines whether to return list of posts or channel-feed object.
   *
   * @returns {Promise.<Object[]>} containing retrieved feeds.
   */
  bulkFeedRetrieval(channelIDs, since = 'y', pretty = true) {
    return new Promise((resolve, reject) => {
      const queue = _.map(channelIDs, id => this.getFeed(id, since));

      Promise.all(queue)
        .then(feeds => {
          logger.info(
            `@FB#bulkFeeds :: successfully retrieved feeds of ${channelIDs.length} channels`
          );
          const results = _.flatten(feeds);

          if (!pretty) {
            resolve(results);
          } else {
            const posts = _.reduce(results, (acc, chunk) => {
              const items = _.map(chunk.response, post => {
                post.parent = chunk.query;
                post.type = 'post';
                return post;
              });
              acc = _.concat(acc, items);
              return acc;
            }, []);

            resolve(posts);
          }
        })
        .catch(error => {
          const prettyError = JSON.stringify(error, null, 2);
          winston.error(
            `- @FB#bulkFeeds :: feeds retrieval failure -> ${prettyError}`
          );

          reject({ code: 500, message: error.message, extensive: prettyError });
        });
    });
  }
}

module.exports = Facebook;
