/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const _ = require('lodash');
const striptags = require('striptags');
const tools = require('./tools');

/** Class representing a webpage */
class Post {
  /**
   * Instantiate the webpage item.
   */
  constructor() {
    this._author = null;          // post author's name
    this._authorUrl = null;       // URL of post's author
    this._channel = null;         // the channel containing this post
    this._channelTitle = null;    // The name of the channel where this post was published
    this._postType = null;        // post type
    this._url = null;             // the URL of this post
    this._date = null;            // publication date of this post
    this._text = null;            // text contents of this posts
    this._location = null;        // related location
    this._position = null;        // post's position within either search results or channel feed
    this._language = null;        // post's language
    this._talking = null;         // # of people talking about this post (FB-specific)
    this._followers = false;      // # of followers of the channel containing this post
    this._verified = false;       // if the channel containing this post has an authentic identity
    this._remove = false;         // if the post is subject to removal
    this._info = {
      reposts: 0,                 // # of post shares
      likes: 0,                   // # of post likes
      comments: 0,                // # of post comments
    };
    this._media =  {
      pictures: [],               // detected pictures
      links: [],                  // detected enclosed links
      videos: [],                 // detected videos
      hashtags: [],               // parsed hashtags
    };
    this._updateRate = 0;         // post's SERP stability
    this._accuracy = 0;           // post's accuracy
    this._potency = 0;            // post's impact
  }

  /**
   * Set post's accuracy rate.
   * @param {number} rate - Post's accuracy rate.
   */
  set accuracy(rate) {
    this._accuracy = Math.round(rate * 100);
  }

  /**
   * Get post's accuracy.
   * @returns {number}
   */
  get accuracy() {
    return this._accuracy;
  }

  /**
   * Set name of post's publisher.
   * @param {string} name - Name of the post's publisher.
   */
  set author(name) {
    this._author = tools.prettyPrint(name);
  }

  /**
   * Set URL of post's publisher.
   * @param {string} link - URL to store.
   */
  set authorUrl(link) {
    this._authorUrl = link;
  }

  /**
   * Set URL of the channel containing this post.
   * @param {string} url - URL to store.
   */
  set channel(url) {
    this._channel = url;
  }

  /**
   * Get URL of the channel containing this post.
   * @returns {string}
   */
  get channel() {
    return this._channel;
  }

  /**
   * Set name of the channel containing this post.
   * @param {string} name - The name to store.
   */
  set channelTitle(name) {
    this._channelTitle = tools.prettyPrint(name);
  }

  /**
   * Get name of the channel containing this post.
   * @returns {string} - Name of the channel containing this post.
   */
  get channelTitle() {
    return tools.shorten(this._channelTitle);
  }

  /**
   * Store # of post comments.
   * @param {number} count - # of comments to store.
   */
  set comments(count) {
    this._info.comments = count;
  }

  /**
   * Set post's publication date.
   * @param {string} dateString - The publication date to store.
   */
  set date(dateString) {
    try {
      this._date = new Date(dateString).toISOString();
    } catch (err) {
      console.error(`@POST :: Invalid date string -> ${dateString}`);
      this._date = new Date(_.now()).toISOString();
    }
  }

  /**
   * Set # of channel followers.
   * @param {number} count - # of people to follow the channel containing this post.
   */
  set followers(count) {
    this._followers = count;
  }

  /**
   * Get full version of post's textual contents.
   * @returns {string} - Post's textual contents (untamed and uncut :)
   */
  get fullText() {
    return tools.prettyPrint(this._text);
  }

  /**
   * Store parsed hashtags.
   * @param {string[]} words - detected hashtags.
   */
  set hashtags(words) {
    _.forEach(words, w => this._media.hashtags.push(w))
  }

  /**
   * Set post's position within SERP or channel feed.
   * @param {!number} position - Post's position.
   */
  set index(position) {
    this._position = position + 1;
  }

  /**
   * Get post's full description.
   * @returns {Object} Full post data.
   */
  get info() {
    return {
      author: this._author,
      authorUrl: this._authorUrl,
      postType: this._postType,
      link: this._url,
      channelTitle: this.channelTitle,
      channel: this.channel,
      followers: this._followers,
      verified: this._verified,
      date: this._date,
      text: this.text,
      location: this._location,
      index: this._position,
      language: this._language,
      info: this._info,
      media: this._media,
      updateRate: this._updateRate,
      accuracyRate: this._accuracy,
      potentialRate: this._potency,
    }
  }

  /**
   * Set post's language
   * @param {string} code - ISO 631 Alpha-2 language code.
   */
  set language(code) {
    this._language = (code && code.length > 2)
      ? code.split('-')[0].toLocaleLowerCase()
      : code.toLocaleLowerCase();
  }

  /**
   * Store # of post likes.
   * @param {number} count - # of likes to store.
   */
  set likes(count) {
    this._info.likes = count;
  }

  /**
   * Set post's URL.
   * @param {string} url - URL of this post.
   */
  set link(url) {
    this._url = url;
  }

  /**
   * Store enclosed URLs.
   * @param {string[]} urls - URLs to store.
   */
  set links(urls) {
    (_.isArray(urls))
      ? _.forEach(urls, url => this._media.links.push(url))
      : this._media.links.push(urls);
  }

  /**
   * Store location data related to post.
   * @param {string} geoString - Location data.
   */
  set location(geoString) {
    this._location = tools.prettyPrint(geoString);
  }

  /**
   * Store location data related to post.
   * @returns {string} Location data.
   */
  get location() {
    return this._location;
  }

  /**
   * Store enclosed Pictures.
   * @param {string[]} urls - Detected picture URLs .
   */
  set pictures(urls) {
    (_.isArray(urls))
      ? _.forEach(urls, url => this._media.pictures.push(url))
      : this._media.pictures.push(urls);
  }

  /**
   * Set the type of this post.
   * @param {string} type - post type.
   */
  set postType(type) {
    const textualTypes = ['question', 'tweet', 'event', 'post', 'text', 'status'];
    if (textualTypes.indexOf(type) !== -1) {
      this._postType = 'text';
    } else if (type === 'picture' || type === 'photo') {
      this._postType = 'photo';
    } else if (type === 'article' || type === 'link') {
      this._postType = 'link';
    } else {
      this._postType = type;
    }
  }

  /**
   * Set post's impact.
   * @param {number} rate - Post's potential influence.
   */
  set potency(rate) {
    this._potency = Math.round(rate);
  }

  /**
   * Return post's potential influence.
   * @returns {number}
   */
  get potency() {
    return this._potency;
  }

  /**
   * Set this post to be subject of removal.
   * @param {boolean} bool - If it's necessary to remove this post from results.
   */
  set remove(bool) {
    this._remove = bool;
  }

  /**
   * Shows if this post is the subject of removal.
   * @returns {boolean} - Removal flag.
   */
  get remove() {
    return this._remove;
  }

  /**
   * Store # post shares.
   * @param {number} count - # of post shares.
   */
  set reposts(count) {
    this._info.reposts = count;
  }

  /**
   * Store post contents.
   * @param {string} contents - Post's textual content to be store.
   */
  set text(contents) {
    this._text = tools.prettyPrint(striptags(contents));
  }

  /**
   * Get shortened version of post's contents.
   * @returns {string} - First 140 symbols of post's text.
   */
  get text() {
    return tools.shorten(this._text);
  }

  /**
   * Set post feed position stability.
   * @param {number} rate - Update rate of the source channel.
   */
  set updateRate(rate) {
    this._updateRate = Math.round(rate * 100);
  }

  /**
   * Set whether this channel is officially verified.
   * @param {boolean} bool - The flag representing if the source channel is official.
   */
  set verified(bool) {
    this._verified = bool;
  }

  /**
   * Get to know if the channel containing the post is officially verified.
   * @returns {boolean} - verification status.
   */
  get verified() {
    return this._verified;
  }

  /**
   * Store enclosed videos.
   * @param {sting[]} urls - URLs of the enclosed videos.
   */
  set videos(urls) {
    (_.isArray(urls))
      ? _.forEach(urls, url => this._media.videos.push(url))
      : this._media.videos.push(urls);
  }

}

module.exports = Post;
