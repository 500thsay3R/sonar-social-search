/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
/* eslint new-cap: 0 */
'use strict';

const _ = require('lodash');
const cld = require('cld');
const czechStem = require('czech-stemmer');
const moment = require('moment');
const natural = require('natural');
const ukrStem = require('ukrstemmer');
const winston = require('winston');

const EnhancedSet = require('./enhanced-set');
const TwitterClient = require('./twitter-api');
const Post = require('./post');
const twitter = new TwitterClient(process.env.TWITTER_ACCESS_TOKEN);

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

class TwitterSearch {
  /**
   * @param parameters - The set of parameters defining an instance of analysis.
   * @param {string[]} parameters.keys - Secondary search keys.
   * @param {!string} parameters.query - The main search key.
   * @param {?string} [parameters.locale=null] - ISO 3166 Alpha-2 country code.
   * @param {string[]} [parameters.inclusions=[]] - The set of plus-words.
   * @param {string[]} [parameters.exclusions=[]] - The set of minus-words.
   */
  constructor(parameters) {
    const { query, locale = 'global', period = 'y', keys = [], settings = {} } = parameters;
    const { include = [], exclude = [] } = settings;

    /** Converts either items or set of items to lower case */
    function lowerCase(item) {
      if (typeof item === 'string') {
        return item.toLocaleLowerCase();
      }
      return _.map(item, element => element.toLocaleLowerCase());
    }

    this._exclusions = lowerCase(exclude);
    this._excsTokens = new EnhancedSet();
    this._inclusions = lowerCase(include);
    this._incsTokens = new EnhancedSet();
    this._keywords = lowerCase(keys);
    this._keysTokens = new EnhancedSet();
    this._language = null;
    this._locale = locale;
    this._period = period;
    this._query = lowerCase(query);
    this._queryTokens = new EnhancedSet();
    this._results = [];
  }

  /**
   * Get analysis minus-words.
   * @returns {String[]} The minus-words.
   */
  get exclusions() {
    return this._exclusions;
  }

  /**
   * Set tokenized version of minus-words.
   * @param {string[]} words - The minus-words to store.
   */
  set excsTokens(words) {
    this._excsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of minus-words.
   * @returns {string[]} Tokenized minus-words.
   */
  get excsTokens() {
    return [...this._excsTokens];
  }

  /**
   * Get analysis plus-words.
   * @returns {String[]} The plus-words.
   */
  get inclusions() {
    return this._inclusions;
  }

  /**
   * Set tokenized version of plus-words.
   * @param {string[]} words - The plus-words to store.
   */
  set incsTokens(words) {
    this._incsTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of plus-words.
   * @returns {string[]} Tokenized plus-words.
   */
  get incsTokens() {
    return [...this._incsTokens];
  }

  /**
   * Get analysis additional keywords.
   * @returns {string[]} Additional keywords.
   */
  get keys() {
    return this._keywords;
  }

  set language(code) {
    this._language = code;
  }

  /**
   * Set tokenized version of additional search keys.
   * @param {string[]} words - The additional search keys to store.
   */
  set keysTokens(words) {
    this._keysTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get tokenized versions of additional search keys.
   * @returns {string[]} Tokenized additional search keys.
   */
  get keysTokens() {
    return [...this._keysTokens];
  }

  /**
   * Get analysis locale settings.
   * @returns {?string} - ISO 3166 Alpha-2 country code.
   */
  get locale() {
    return this._locale;
  }

  set results(object) {
    this._results.push(object);
  }

  get results() {
    return this._results;
  }

  /**
   * Get all the search parameters.
   * @returns {object} The search parameters.
   */
  get parameters() {
    return {
      query: this._query,
      queryTokens: this.queryTokens,
      keywords: this._keywords,
      keysTokens: this.keysTokens,
      locale: this._locale,
      language: this._language,
      inclusions: this.inclusions,
      incsTokens: this.incsTokens,
      exclusions: this.exclusions,
      excsTokens: this.excsTokens,
    };
  }

  /**
   * Get main search key.
   * @returns {string} The main search key.
   */
  get query() {
    return this._query;
  }

  get period() {
    return this._period;
  }

  /**
   * Set tokenized version of the main search key.
   * @param {string[]} words - Query tokens.
   */
  set queryTokens(words) {
    this._queryTokens.bulkAdd(_.flatten(words));
  }

  /**
   * Get main search query tokens.
   * @returns {string[]} - Main search key tokens.
   */
  get queryTokens() {
    return [...this._queryTokens];
  }

  /**
   * Detect language of the provided textual chunk.
   * @param {String|String[]} chunk - Some text or tokens to get language of.
   * @param {string} hint - ISO 3166 Alpha-2 country code
   * @returns {Promise}
   */
  static detectQueryLang(chunk, hint) {
    return new Promise(resolve => {
      // stringify incoming chunk
      const text = (_.isArray(chunk))
        ? chunk.join(' ')
        : chunk;

      // add tld hint if locale preset has been included
      const options = {};
      if (hint && hint.length && hint !== 'global') {
        options.tldHint = hint;
      }

      cld.detect(text, options, (error, result) => {
        if (result && !error) {
          const languages = _.compact(result.languages);
          resolve(languages[0].code || 'en');
        } else {
          resolve('en');
        }
      });
    });
  }

  /**
   * Tokenize incoming words and sentences.
   * @param {string|string[]} item - Some standalone word ar a set of words to be tokenized.
   * @param {string} lang - Language of the incoming item.
   * @returns {string|string[]} Stemmed tokens.
   */
  static createTokens(item, lang) {
    // languages supported by natural#PorterStemmer
    const fullySupported = ['it', 'fr', 'es', 'fa', 'pt'];
    // languages supported by natural#Stemmer
    const semiSupported = ['ja', 'pl'];
    let tokenizer;

    if (lang in fullySupported) {
      tokenizer = natural[`PorterStemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang in semiSupported) {
      tokenizer = natural[`Stemmer${_.capitalize(lang)}`].tokenizeAndStem;
    } else if (lang === 'en') {
      tokenizer = natural.PorterStemmer.tokenizeAndStem;
    } else if (lang === 'ru') {
      tokenizer = natural.PorterStemmerRu.tokenizeAndStem;
    } else if (lang === 'sv' || lang === 'nn' || lang === 'nb') {
      tokenizer = natural.PorterStemmerNo.tokenizeAndStem;
    } else if (lang === 'uk') {
      tokenizer = ukrStem;
    } else if (lang === 'cs') {
      tokenizer = czechStem;
    } else {
      tokenizer = _.words;
    }

    const text = (typeof item === 'string')
      ? item
      : item.join(' ');

    const results = tokenizer(text);
    logger.debug(`@CREATE-TOKENS :: DONE [${lang}] -> ${results}`);
    return results;
  }

  /**
   * Set up search parameters by defining language of search keys and tokenize them.
   * @param {string[]} exclusions - The minus-words.
   * @param {string[]} inclusions - The plus-words.
   * @param {string[]} keys - Secondary search keys.
   * @param {string} query - Primary search key
   * @returns {Promise}
   * @private
   */
  _processParameters(query, keys, inclusions, exclusions) {
    const compoundQuery = _.union(keys, inclusions, exclusions);

    return TwitterSearch.detectQueryLang(compoundQuery, this._locale)
      .then((languageCode) => {
        // set up search parameters
        this.keysTokens = TwitterSearch.createTokens(keys, this._language);
        this.incsTokens = TwitterSearch.createTokens(inclusions, this._language);
        this.excsTokens = TwitterSearch.createTokens(exclusions, this._language);
        this.queryTokens = TwitterSearch.createTokens(query, this._language);
        this.language = languageCode;
      });
  }

  /**
   * Processes post textual contents.
   * @param {Object} object - Set of properties representing a tweet.
   * @param {number} index - Tweet's position within Twitter Search API search results.
   * @returns {Promise.<Post>} Processed tweet.
   * @private
   *
   * @property created_at - Tweet publication date.
   * @property screen_name - Tweet's publisher screen alias.
   * @property id_str - Twitter entity ID formatted as a string.
   * @property full_name - Full name of some Twitter entity.
   * @property retweet_count - # of tweet shares.
   * @property favorite_count - # of tweet likes.
   * @property statuses_count - # of statuses generated by tweet's publisher.
   * @property followers_count - # of Twitter users following tweet publisher.
   * @property entities.urls - URLs enclosed in the tweet.
   * @property media_url - Enclosed media URL.
   * @property expanded_url - Full version of the URL enclosed in the tweet.
   */
  static _processTweetContent(object, index) {
    const tweet = new Post();

    tweet.author = `@${object.user.screen_name}`;
    tweet.authorUrl = `https://twitter.com/${object.user.screen_name}`;
    tweet.postType = 'tweet';
    tweet.link = `https://twitter.com/${object.user.screen_name || object.user.id}` +
      `/status/${object.id_str}`;
    tweet.date = object.created_at;
    tweet.text = object.text;
    tweet.channel = `https://twitter.com/${object.user.screen_name || object.user.id_str}`;
    tweet.channelTitle = object.user.screen_name;

    if (object.place) {
      tweet.location = `${object.place.full_name || ''}${` ${object.place.country}` || ''}`;
    }

    tweet.index = index;
    tweet.reposts = object.retweet_count || 0;

    const timeDiff = _.now() - new Date(object.user.created_at).getTime();
    const daysActive = Math.round(new moment.duration(timeDiff).asDays());
    tweet.updateRate = object.user.statuses_count / (daysActive * 100);
    tweet.followers = object.user.followers_count;

    tweet.likes = object.favorite_count || 0;

    tweet.comments = 0;
    tweet.verified = false;
    tweet.language = object.lang;

    if (object.entities) {
      tweet.hashtags = _.map(object.entities.hashtags, tag => `#${tag.text}`);
      tweet.pictures = _.map(object.entities.media, item => item.media_url);
      tweet.links = _.map(object.entities.urls, item => `${item.expanded_url}`);
    }
    if (object.pictures) {
      tweet.pictures = object.pictures;
    }

    if (object.links) {
      tweet.links = object.link;
    }

    if (object.type === 'video') {
      tweet.videos = object.link;
    }

    return Promise.resolve(tweet);
  }

  /**
   * Scan if some set of keywords is indeed present within Facebook post.
   * @param {string} body - Facebook post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @param {boolean} inTitle - Reveals if the scan is to be perform within post's title.
   * @param {boolean} partial - Reveals if the scan is being performed on tokenized keywords.
   * @returns {Promise}
   * @private
   */
  _scanHits(body, items, tokenized, type, inTitle, partial = false) {
    return new Promise(resolve => {
      const target = body.toLowerCase();
      let result;
      let stake;

      if (type === 'query') {
        // check if there's any occurrence of the `query`-items within webpage
        result = _.every(items, i => target.indexOf(i) !== -1);
        stake = (inTitle) ? 0.4 : 0.15;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * +result
        );
      } else if (type === 'keys') {
        // count how many keywords of `keys` type are present within the webpage
        result = _.reduce(items, (counter, i) => {
          return (target.indexOf(i) !== -1)
            ? counter++
            : counter;
        }, 0);
        stake = (inTitle) ? 0.35 : 0.1;

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : stake * (1 - 0.3 * +partial) * (+result / items.length)
        );
      } else if (type === 'inclusions') {
        // check if all the `inclusions` are contained within the webpage
        result = _.every(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      } else {
        // check if none of the `exclusions` are contained within the webpage
        result = _.some(items, i => target.indexOf(i) !== -1);

        resolve(
          (!result && !partial)
            ? this._scanHits(target, tokenized, null, type, inTitle, true)
            : result
        );
      }
    });
  }

  /**
   * Search for analysis-defining keywords within the Facebook post.
   * @param {string} title - Facebook post's title (i.e. author's name)
   * @param {string} body - Facebook post's body containing text.
   * @param {string[]} items - Keywords the post to be scanned for.
   * @param {string[]} tokenized - Tokenized version of `items`.
   * @param {string} type - Describes type of keywords the post to be scanned for.
   * @returns {Promise}
   * @private
   */
  _scanTweet(title, body, items, tokenized, type) {
    return Promise.all([
      this._scanHits(title, items, tokenized, type, true),
      this._scanHits(body, items, tokenized, type, false),
    ]);
  }

  /**
   * Evaluate to what extent does some page comply to search request.
   * @returns {Promise}
   * @private
   */
  _evaluateAccuracy(post, description, parameters) {
    const title = description.toLocaleLowerCase();

    const body = post.fullText.toLocaleLowerCase();

    return Promise.all([
      this._scanTweet(title, body, [parameters.query], parameters.queryTokens, 'query'),
      this._scanTweet(title, body, parameters.keys, parameters.keysTokens, 'keys'),
      this._scanTweet(title, body, this.inclusions, this.incsTokens, 'inclusions'),
      this._scanTweet(title, body, this.exclusions, this.excsTokens, 'exclusions'),
    ]).then(data => {
      const [,, inclusions, exclusions] = data;
      const marks = _.flatten(_.slice(data, 0, 2));
      const rate = _.reduce(marks, (m, value) => m += value, 0);

      const proceed = (
        _.every(inclusions, i => i === true) && _.every(exclusions, i => i === false)
      );

      return Promise.resolve({ rate, proceed });
    });
  }

  /**
   * Evaluate Facebook post's potential influence.
   * @param {Object} post - Set of properties representing Facebook post.
   * @param {Object} channel - Set of properties representing Facebook channel.
   * @returns {Promise.<number>} Calculated social potential rate.
   * @private
   */
  static _evaluatePotency(post) {
    const timeDiff = _.now() - new Date(post._date).getTime();
    const timePassed = Math.round(new moment.duration(timeDiff).asHours());
    const likes = (post._info.likes + 1) / 1000;
    const reposts = post._info.reposts / 1000;
    const members = post._followers / 1000;
    const speed = post._updateRate;
    const position = post._position;
    let rate = 0;

    if (timePassed * 0.0003366 < 0.8) {
      const currentArea = ((0.04 + (timePassed * 0.0003366)) *
        (likes + 4 * reposts)) *
        Math.pow(members, 2) + (0.96 - (timePassed * 0.0003366)) *
        (1 / ((speed + 1) * position * position)) * members;

      const idealArea = (likes + 4 * reposts) *
        Math.pow(members, 2);

      const socialPotential = Math.round((1 - (currentArea / idealArea)) * 100);

      rate = socialPotential < 0 ? 100 : socialPotential;
    }

    return Promise.resolve(rate);
  }

  /**
   * Calculate secondary post rates.
   * @param {Object} tweet - Set of properties representing Facebook post.
   * @param {Object} description - Textual description of the tweet publisher.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _evaluateTweetRates(tweet, description, parameters) {
    return Promise.all([
      this._evaluateAccuracy(tweet, description, parameters),
      TwitterSearch._evaluatePotency(tweet),
    ])
      .then(data => {
        const [accuracy, potency] = data;
        const proceed = accuracy.proceed;
        tweet.accuracy = accuracy.rate;
        tweet.potency = potency;

        if (proceed && tweet.accuracy > 10 && tweet.potency > 10) {
          this.results = tweet.info;
        }
      })
      .catch(error => {
        console.log(`@POST-ANALYSIS :: post rates processing failure -> ${error}`);
        console.log(error.stack);
        return null;
      });
  }

  /**
   * Perform Facebook posts processing.
   * @param {Object} item - Set of properties representing Tweet.
   * @param {number} index - Tweet's position within search results.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise.<Promise>}
   * @private
   */
  _evaluateTweet(item, index, parameters) {
    const description = `${item.user.name || ''} ${item.user.description || ''}`;
    return TwitterSearch._processTweetContent(item, index)
      .then(tweet => this._evaluateTweetRates(tweet, description, parameters));
  }

  /**
   * Perform parallel tweets processing.
   * @param {Object[]} tweets - Set of Tweet feeds.
   * @param {Object} parameters - Analysis-defining parameters.
   * @returns {Promise}
   * @private
   */
  _processTweets(tweets, parameters) {
    const tasks = _.map(tweets, (t, index) => this._evaluateTweet(t, index, parameters));

    return Promise.all(tasks);
  }

  /**
   * Perform global leads retrieval
   * @returns {Promise}
   */
  search() {
    return this._processParameters(this._query, this._keywords, this._inclusions, this._exclusions)
      .then(() => twitter.search(this.query, this.locale, 'tweet', this.period))
      .then(data => this._processTweets(data.response, this.parameters))
      .then(() => {
        if (this.results && !this.results.length) {
          logger.info(`@TW#search :: [${this.query} --${this.period}] -> **NO** leads retrieved`);
          throw new Error('No leads retrieved');
        }

        logger.info(`@TW#search :: [${this.query} --${this.period}] -> ${this.results.length} ` +
          'leads retrieved.');

        return this.results;
      })
      .catch(error => {
        logger.error(`@TW#search :: ALERT -> ${error.message || error}`);

        if (error && error.message !== 'No leads retrieved') {
          logger.error(error.stack);
          return { error: { code: 1, message: error.message } };
        }

        return { error: { code: 0, message: 'No leads retrieved' } };
      });
  }
}

module.exports = TwitterSearch;
