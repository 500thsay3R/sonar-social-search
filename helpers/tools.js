/* eslint-env; node, es6 */
/* eslint strict: 0 */
'use strict';

const _ = require('lodash');

/**
 * Replace extra whitespaces, HTML special characters nd escape symbols.
 * @param {string} text - Some text to make look pretty.
 * @returns {*} Pretty text.
 */
function prettyPrint(text) {
  return (typeof text === 'string' && text.length)
    ? text.replace(/\r|\n|\t/ig, ' ')
    .replace(/&amp;/ig, '&')
    .replace(/&quot;/ig, '"')
    .replace(/&gt;/ig, '>')
    .replace(/&lt;/ig, '<')
    .replace(/&nbsp;/ig, ' ')
    .replace(/\s+/ig, ' ')
    .trim()
    : text;
}

/**
 * Shorten the given text to 140 symbols
 * @param {string} string - Some text to shorten.
 * @returns {*} Shortened text.
 */
function shorten(string) {
  const text = prettyPrint(string);
  if (typeof text === 'string' && text.length) {
    return (text.length <= 140)
      ? text
      : `${text.substring(0, 140)}...`;
  }

  return text;
}

/**
 * Checks whether any of the provided tokens exists within the target string.
 * @param {string} string - The string to be analyzed.
 * @param {string[]} tokens - The keywords to look for.
 * @returns {boolean}
 */
function find(string, tokens) {
  const text = prettyPrint(string).toLocaleLowerCase();
  return _.some(tokens, t => text.indexOf(t) !== -1);
}

module.exports = {
  prettyPrint,
  find,
  shorten,
};
