/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-return-assign: 0 */
'use strict';


const _ = require('lodash');
const querystring = require('querystring');
const request = require('request');
const url = require('url');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

class Twitter {
  constructor(accessToken) {
    this._token = accessToken;
    this.DAILY_SCAN_WINDOW = 1296e5; // 36h time frame
    this.FULL_SCAN_WINDOW = 8e9; // 3 months time frame
  }

  /**
   * Convert Twitter API response from string to JSON.
   * @param {string} responseBody - Twitter API response
   * @returns {Promise.<Object>} The response JSON.
   * @private
   */
  _parseAPIResponse(responseBody) {
    return new Promise((resolve, reject) => {
      try {
        resolve(JSON.parse(responseBody));
      } catch (parseError) {
        logger.error(`@TW :: failed to parse Twitter API response -> ${parseError}.`);

        reject(new Error('Unexpected Twitter API response'));
      }
    })
      .then(responseObject => {
        if (responseObject.errors) {
          throw new Error(responseObject.errors[0].message);
        } else {
          return responseObject;
        }
      });
  }

  /**
   * Performs atomic Twitter requests.
   * @private
   * @param {string} link - URL of API endpoint.
   * @param {Object} headers - The request headers.
   *
   * @returns {Promise.<Object>} containing parsed Twitter request.
   * @rejects if either request or response parsing fails.
   */
  _performAPIRequest(link, headers) {
    return new Promise((resolve, reject) => {
      request({
        url: link,
        headers,
        method: 'GET',
      }, (error, response) => {
        if (response && response.body && !error) {
          this._parseAPIResponse(response.body)
            .then(dataObject => resolve(dataObject))
            .catch(errorObject => reject(errorObject));
        } else {
          logger.error(
            `@TW :: API request failed -> ${(response && response.body) ? response.body : error}`
          );

          reject(new Error('Failed to establish connection with Twitter API'));
        }
      });
    });
  }

  /**
   * Performs successive Twitter Search API request and response processing.
   * @access private
   * @param {Object} parameters - Collection of request criteria.
   * @param {string} parameters.url - API endpoint url to call.
   * @param {string} parameters.query - The search query.
   * @param {string} [parameters.locale] - ISO 3166 alpha 2 country code.
   * @param {string} parameters.type - The entity type to retrieve.
   * @param {string} [parameters.target] - Target language of the search query.
   * @param {number} [tries] - # of tries left.
   * @param {number} [pages] - Defines how many pages of data to retrieve.
   * @param {Object[]} [results] - Accumulates results.
   *
   * @property search_metadata - Twitter Search meta data.
   * @property next_results - Twitter Search API next page token.
   *
   * @returns {Promise.<Object>} containing retrieved data.
   * @rejects if any error occurs while data extraction.
   */
  _apiRequest(parameters, pages = 2, tries = 4, results = []) {
    const { query, market = 'global', type } = parameters;

    return new Promise((resolve) => {
      const headers = {
        Authorization: `Bearer ${this._token}`,
        'Content-Encoding': 'utf-8',
      };

      this._performAPIRequest(parameters.url, headers)
        .then(response => {
          const accumulator = _.concat(results, response.statuses);

          // jump up to the next page of results if it's not prohibited by `pages` parameter
          const nextPage = response.search_metadata && response.search_metadata.next_results &&
            (pages > 0 || pages === Infinity);

          if (nextPage) {
            const newUrl = parameters.url.split('?')[0] + response.search_metadata.next_results;
            parameters.url = encodeURI(newUrl);

            resolve(this._apiRequest(parameters, tries, --pages, accumulator));
          } else {
            if (accumulator && accumulator.length) {
              logger.debug(
                `@FB#apiRequest :: <"${query}" --${market}> -> ` +
                `${accumulator.length} ${accumulator.length % 10 === 1 ? type : `${type}s`}` +
                ' retrieved.'
              );
            }

            resolve({ query, market, type, response: accumulator });
          }
        })
        .catch(error => {
          // if some weird FB server-related stuff occurred, try 3 more times
          if (error.code && error.code.toString()[0] === '5' && tries >= 3) {
            logger.error(
              `@TWI#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Repeating request... Attempt: (${4 - (tries - 1)}/4)`
            );

            resolve(this._apiRequest(parameters, pages, --tries));
          } else if (error.code && error.code.toString()[0] === '5' && tries < 3 && tries > 0) {
            logger.error(
              `@TWI#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `Retrying... Attempt: (${4 - (tries - 1)}/4)`
            );

            resolve(this._apiRequest(parameters, pages, --tries));
          } else {
            // if there's nothing can be done, resolve with results that have previously been
            // grabbed
            logger.error(
              `@TWI#apiRequest :: <"${query}" --${market}> :: request failed. -> ` +
              `REASON: ${error.message || error}`
            );

            resolve({ query, market, type, response: results || [] });
          }
        });
    });
  }

  /**
   * Performs Google+ publications search.
   * @param {string} query - Primary search key.
   * @param {string} [market='global'] - Target country preset.
   * @param {string} [type='tweet'] - Twitter entity to look for.
   * @param {string} [period='y'] - Twitter search timeframe.
   * @returns {Promise.<Object>} Google+ API response.
   */
  search(query, market = 'global', type = 'tweet', period = 'y') {
    const searchRequest = {
      q: query,                         // the search key
      result_type: (period === 'y')     // defines what type of results to retrieve - recent or
        ? 'mixed'                       // mixed (recent + relevant)
        : 'recent',
      count: 100,                       // # of result items to extract
    };

    if (market !== 'global') {
      searchRequest.locale = market;    // currently only Japanese preset is effective
    }

    // specify since what date posts are to be retrieved
    if (period === 'd') {
      const timeFrame = _.now() - 864e5;
      const date = (new Date(timeFrame)).toISOString().slice(0, 10);
      searchRequest.q += ` since:${date}`;
    }

    const endpoint = {
      protocol: 'https:',
      host: 'api.twitter.com',
      pathname: '/1.1/search/tweets.json',
      search: querystring.stringify(searchRequest),
    };

    return this._apiRequest({ url: url.format(endpoint), query: searchRequest.q, market, type });
  }
}

module.exports = Twitter;
