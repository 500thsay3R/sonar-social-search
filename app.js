/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint new-cap: 0 */
/* eslint no-param-reassign: 0 */
'use strict';

require('newrelic');

const bodyParser = require('body-parser');
const compression = require('compression');
const DDPServer = require('@sonar/sonar-ddp-handler');
const express = require('express');
const formidable = require('formidable');
const fs = require('fs');
const http = require('http');
const path = require('path');
const winston = require('winston');

const FacebookSearch = require('./helpers/facebook-search');
const TwitterSearch = require('./helpers/twitter-search');
const GPlusSearch = require('./helpers/gplus-search');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const router = express.Router();
const app = express();

app.set('port', process.env.PORT || '3000');
app.use(bodyParser.json());
app.use(compression());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', router);

/**
 * HTTP handlers
 */

router.post('/twitter', (req, res) => {
  const form = new formidable.IncomingForm();

  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields) => {
      if (err) {
        reject({ error: { message: 'Invalid request', code: 13 } });
      }
      fields.keys = [fields.query, fields.keys];
      resolve(fields);
    });
  })
    .then(data => {
      const tw = new TwitterSearch(data);
      return tw.search();
    })
    .then(results => {
      res.status(!results.error ? 200 : 500).send(`<pre>${JSON.stringify(results, null, 2)}</pre>`);
    })
    .catch(() => {
      const text = '<pre>{ error: { message: "No leads retrieved", code: 10 }</pre>';
      res.status(500).send(text);
    });
});

router.get('/status', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

router.get('/test', (req, res) => {
  fs.readFile(path.join(__dirname, '/public/test.html'), (err, data) => {
    res.writeHeader(200, { 'Content-Type': 'text/html', 'Content-Length': data.length });
    res.write(data);
    res.end();
  });
});

/**
 * DDP methods
 */
app.server = http.createServer(app);
const server = new DDPServer({ httpServer: app.server });

server.methods({
  facebook: function facebook(req) {
    return new Promise((resolve) => {
      const fb = new FacebookSearch(req);

      fb.search()
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          const errorBody = { error: { code: 1, message: error.message } };

          if (error && error.message === 'Google ban') {
            errorBody.error.code = 2;
          }
          resolve(errorBody);
        });
    });
  },
  twitter: function twitter(req) {
    return new Promise((resolve) => {
      const tw = new TwitterSearch(req);

      tw.search()
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          const errorBody = { error: { code: 1, message: error.message } };

          if (error && error.message === 'Google ban') {
            errorBody.error.code = 2;
          }
          resolve(errorBody);
        });
    });
  },
  gplus: function gplus(req) {
    return new Promise((resolve) => {
      const gp = new GPlusSearch(req);

      gp.search()
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          const errorBody = { error: { code: 1, message: error.message } };

          if (error && error.message === 'Google ban') {
            errorBody.error.code = 2;
          }
          resolve(errorBody);
        });
    });
  },
});

app.server.listen(app.get('port'), () => {
  logger.info(`SONAR SOCIAL SEARCH is up & running on port #${app.get('port')}`);
});

module.exports = app;
